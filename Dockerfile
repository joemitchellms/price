FROM openjdk:11.0.6-jre-buster
VOLUME /tmp
COPY ./build/libs/*.jar app.jar
ENV SPRING_OUTPUT_ANSI_ENABLED=ALWAYS JAVA_OPTS=""
ADD entrypoint.sh entrypoint.sh
ENTRYPOINT ["./entrypoint.sh"]
