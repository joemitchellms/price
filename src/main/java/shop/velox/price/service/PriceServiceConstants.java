package shop.velox.price.service;

public final class PriceServiceConstants {

  public static class Authorities {

    public static final String PRICE_ADMIN = "Admin_Price";
    public static final String CURRENCY_ADMIN = "Admin_Currency";

  }

  private PriceServiceConstants() {
    // private constructor to prevent instantiation
  }
}
