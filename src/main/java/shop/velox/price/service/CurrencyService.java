package shop.velox.price.service;

import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import shop.velox.price.model.CurrencyEntity;

/**
 * Executes CRUD operations on Currency
 */
public interface CurrencyService {

  /**
   * Creates a currency and returns it
   *
   * @param currency the currency to be created
   * @return the just created currency
   */
  @PreAuthorize("@veloxAuthorizationEvaluator.hasGlobalOrCustomAdminAuthority(authentication,"
      + "T(shop.velox.price.service.PriceServiceConstants.Authorities).CURRENCY_ADMIN)")
  CurrencyEntity createCurrency(CurrencyEntity currency);

  /**
   * Gets all currencies
   *
   * @return the currencies wrapped in a {@link Page}
   */
  Page<CurrencyEntity> findAll(Pageable pageable);

  /**
   * Gets currency by id
   *
   * @return the currency
   */
  Optional<CurrencyEntity> findOne(String id);

  /**
   * Updates a currency with the given id
   *
   * @param id       the id of the currency to be updated
   * @param currency contains the new values for the currency to be updated
   * @return the updated currency
   */
  @PreAuthorize("@veloxAuthorizationEvaluator.hasGlobalOrCustomAdminAuthority(authentication,"
      + "T(shop.velox.price.service.PriceServiceConstants.Authorities).CURRENCY_ADMIN)")
  CurrencyEntity update(String id, CurrencyEntity currency);

  /**
   * Removes a currency with given id
   *
   * @param id the id of the currency to be remove
   */
  @PreAuthorize("@veloxAuthorizationEvaluator.hasGlobalOrCustomAdminAuthority(authentication,"
      + "T(shop.velox.price.service.PriceServiceConstants.Authorities).CURRENCY_ADMIN)")
  void removeCurrency(String id);

}
