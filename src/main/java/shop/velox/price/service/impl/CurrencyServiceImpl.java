package shop.velox.price.service.impl;

import java.util.Optional;
import javax.validation.ConstraintViolationException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import shop.velox.price.dao.CurrencyRepository;
import shop.velox.price.model.CurrencyEntity;
import shop.velox.price.service.CurrencyService;

@Service
public class CurrencyServiceImpl implements CurrencyService {

  private static final Logger LOG = LoggerFactory.getLogger(CurrencyServiceImpl.class);

  private final CurrencyRepository currencyRepository;

  public CurrencyServiceImpl(@Autowired CurrencyRepository currencyRepository) {
    this.currencyRepository = currencyRepository;
  }

  @Override
  public CurrencyEntity createCurrency(CurrencyEntity currency) {
    try {
      return currencyRepository.save(currency);
    } catch (DataIntegrityViolationException e) {
      throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, "The currency could not "
          + "be created because there is already another currency with the same ISO code", e);
    } catch (ConstraintViolationException e) {
      throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, "The currency could not "
          + "be created because of missing information for ISO code, name or symbol", e);
    }
  }

  @Override
  public Page<CurrencyEntity> findAll(Pageable pageable) {
    return currencyRepository.findAll(pageable);
  }

  @Override
  public Optional<CurrencyEntity> findOne(String id) {
    return currencyRepository.findOneById(id);
  }

  @Override
  public CurrencyEntity update(String id, CurrencyEntity currency) {
    CurrencyEntity existingCurrency = currencyRepository.findOneById(id)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

    existingCurrency.setIsoCode(currency.getIsoCode());
    existingCurrency.setName(currency.getName());
    existingCurrency.setSymbol(currency.getSymbol());

    try {
      return currencyRepository.save(existingCurrency);
    } catch (ConstraintViolationException e) {
      throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, "The currency could not "
          + "be updated because of missing information for ISO code, name or symbol", e);
    } catch (DataIntegrityViolationException e) {
      throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, "The currency could not "
          + "be updated because there is already another currency with the same ISO code", e);
    }
  }

  @Override
  public void removeCurrency(String id) {
    if (StringUtils.isBlank(id)) {
      throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY,
          "Cannot remove item without id");
    }

    LOG.info("removeCurrency {}", id);

    CurrencyEntity currencyEntity = currencyRepository.findOneById(id)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

    try {
      currencyRepository.delete(currencyEntity);
    } catch (DataIntegrityViolationException e) {
      throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, "Could not remove "
          + "currency because it is still used by prices", e);
    }
  }

}
