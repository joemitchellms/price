package shop.velox.price.service.impl;

import static java.time.LocalDateTime.now;
import static org.apache.commons.collections4.CollectionUtils.emptyIfNull;
import static shop.velox.price.dao.PriceSpecification.hasStatus;
import static shop.velox.price.dao.PriceSpecification.isArticleIdIn;
import static shop.velox.price.dao.PriceSpecification.isCurrencyEqual;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Optional;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import shop.velox.price.api.dto.ArticleDto;
import shop.velox.price.api.dto.PriceStatus;
import shop.velox.price.dao.CurrencyRepository;
import shop.velox.price.dao.PriceRepository;
import shop.velox.price.model.CurrencyEntity;
import shop.velox.price.model.PriceEntity;
import shop.velox.price.service.PriceService;

@Service
public class PriceServiceImpl implements PriceService {

  private static final Logger LOG = LoggerFactory.getLogger(PriceServiceImpl.class);

  private final PriceRepository priceRepository;
  private final CurrencyRepository currencyRepository;

  public PriceServiceImpl(@Autowired PriceRepository priceRepository,
      @Autowired CurrencyRepository currencyRepository) {
    this.priceRepository = priceRepository;
    this.currencyRepository = currencyRepository;
  }

  @Override
  public PriceEntity createPrice(final PriceEntity price) {
    CurrencyEntity currency = getCurrency(price.getCurrency().getId());
    price.setCurrency(currency);

    try {
      if (price.getValidFrom().isBefore(price.getValidTo())) {
        return priceRepository.save(price);
      } else {
        throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY,
            "validTo time cannot be after validFrom");
      }

    } catch (DataIntegrityViolationException dive) {
      // It works on MySql, but does not work on Derby.
      if (dive.getMessage().contains(PriceEntity.COMPOSITE_CONSTRAINT_NAME)) {
        LOG.info("cannot create price {}, it already exists", price);
        PriceEntity alreadyExistingPrice = priceRepository.
            findOneByArticleIdAndValidFromAndValidToAndMinQuantityAndUnitPriceAndCurrency(
                price.getArticleId(), price.getValidFrom(), price.getValidTo(),
                price.getMinQuantity(), price.getUnitPrice(), currency)
            .orElseThrow(() -> new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,
                "price not found", dive) {
            });

        if (BooleanUtils.compare(alreadyExistingPrice.isActive(), price.isActive()) != 0) {
          alreadyExistingPrice.setActive(price.isActive());
          return priceRepository.save(alreadyExistingPrice);
        } else {
          return alreadyExistingPrice;
        }
      } else {
        LOG.error("when creating price: {} : {}", price, dive.getMostSpecificCause().getMessage());
        throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY,
            dive.getMostSpecificCause().getMessage());
      }
    }
  }

  @Override
  public void removePrice(final String id) {

    if (StringUtils.isBlank(id)) {
      throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY,
          "Cannot remove item without id");
    }

    LOG.info("removePrice {}", id);

    PriceEntity entity = priceRepository.findOneByIdAndActiveIsTrue(id)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    if (entity.isActive()) {
      entity.setActive(false);
      priceRepository.save(entity);
    }
  }

  @Override
  public PriceEntity update(String id, PriceEntity price) {

    LOG.info("Update {}, {}", id, price);

    if (StringUtils.isNotBlank(price.getId()) && !StringUtils.equals(id, price.getId())) {
      throw new ResponseStatusException(HttpStatus.CONFLICT, "Cannot manually change the price id");
    }

    PriceEntity existingPrice = priceRepository.findOneById(id)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

    if (!StringUtils.equals(existingPrice.getArticleId(), price.getArticleId())) {
      throw new ResponseStatusException(HttpStatus.CONFLICT, "Cannot update the articleId");
    }

    removePrice(id);

    if (!price.isActive()) {
      throw new ResponseStatusException(HttpStatus.NO_CONTENT);
    }

    return createPrice(price);
  }

  @Override
  public Optional<PriceEntity> findById(String id) {
    return priceRepository.findOneById(id);
  }

  @Override
  public Page<PriceEntity> findAll(Pageable pageable, PriceStatus filter, String currencyId) {
    return priceRepository.findAll(
        hasStatus(filter)
            .and(isCurrencyEqual(currencyId)),
        pageable);
  }

  @Override
  public Page<PriceEntity> findAllByArticleIds(Pageable pageable, List<String> articleIds,
      PriceStatus filter, String currencyId) {
    LOG.info("findAllByArticleIds with articleIds: {}, filters: {}, currencyId: {}",
        String.join(", ", emptyIfNull(articleIds)), filter, currencyId);

    return priceRepository.findAll(
        isArticleIdIn(articleIds)
            .and(hasStatus(filter))
            .and(isCurrencyEqual(currencyId)),
        pageable);
  }

  @Override
  public Optional<PriceEntity> findCheapestPriceForArticleAndQuantity(
      String articleId, BigDecimal quantity) {
    return priceRepository
        .findFirstByArticleIdAndActiveIsTrueAndMinQuantityLessThanEqualOrderByUnitPriceAsc(
            articleId, quantity);
  }

  @Override
  public Optional<PriceEntity> getAppliedPrice(String articleId, BigDecimal quantity) {
    Page<PriceEntity> page = priceRepository
        .findAllByArticleIdAndActiveIsTrueAndValidFromIsBeforeAndValidToIsAfterAndMinQuantityIsLessThanEqualOrderByUnitPriceAsc(
            PageRequest.of(0, 1), articleId,
            now(), now(), quantity);
    LOG.debug("getAppliedPrice with articleId: {}, quantity: {} finds {} results", articleId,
        quantity, page.getTotalElements());
    if (CollectionUtils.isNotEmpty(page.getContent())) {
      return Optional.ofNullable(page.getContent().get(0));
    } else {
      return Optional.empty();
    }
  }

  @Override
  public ArticleDto applyPrice(PriceEntity priceEntity, BigDecimal quantity) {
    return new ArticleDto(priceEntity.getArticleId(), quantity,
        quantity.multiply(priceEntity.getUnitPrice()).setScale(2, RoundingMode.DOWN),
        priceEntity.getUnitPrice());
  }

  private CurrencyEntity getCurrency(String currencyId) {
    if (StringUtils.isNotBlank(currencyId)) {
      return currencyRepository.findOneById(currencyId)
          .orElseThrow(
              () -> new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY,
                  "Could not find currency"));
    } else {
      throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, "Could not find currency");
    }
  }

}
