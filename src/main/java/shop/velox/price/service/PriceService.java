package shop.velox.price.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import shop.velox.price.api.dto.ArticleDto;
import shop.velox.price.api.dto.PriceStatus;
import shop.velox.price.model.PriceEntity;


/**
 * Executes CRUD operations on Price
 */
public interface PriceService {

  /**
   * Creates a price and returns it
   *
   * @param price the price to be created
   * @return the just created price
   */
  @PreAuthorize("@veloxAuthorizationEvaluator.hasGlobalOrCustomAdminAuthority(authentication,"
      + "T(shop.velox.price.service.PriceServiceConstants.Authorities).PRICE_ADMIN)")
  PriceEntity createPrice(PriceEntity price);

  /**
   * Removes a price with given id
   *
   * @param id the id of the price to be set to inactive
   */
  @PreAuthorize("@veloxAuthorizationEvaluator.hasGlobalOrCustomAdminAuthority(authentication,"
      + "T(shop.velox.price.service.PriceServiceConstants.Authorities).PRICE_ADMIN)")
  void removePrice(String id);

  /**
   * Updates a price with the given id
   *
   * @param id    the id of the price to be updated
   * @param price contains the new values for the price to be updated
   * @return the updated price
   */
  @PreAuthorize("@veloxAuthorizationEvaluator.hasGlobalOrCustomAdminAuthority(authentication,"
      + "T(shop.velox.price.service.PriceServiceConstants.Authorities).PRICE_ADMIN)")
  PriceEntity update(String id, PriceEntity price);

  /**
   * Gets a price by its id
   *
   * @param id the id of the price
   * @return the price wrapped in a {@link Optional}
   */
  Optional<PriceEntity> findById(String id);

  /**
   * Gets all prices
   *
   * @return the prices wrapped in a {@link Page}
   */
  @PreAuthorize(
      "(#filter != null && #filter.equals((T(shop.velox.price.api.dto.PriceStatus).ACTIVE)) && #currencyId != null) || "
          + "@veloxAuthorizationEvaluator.hasGlobalOrCustomAdminAuthority(authentication, T(shop.velox.price.service.PriceServiceConstants.Authorities).PRICE_ADMIN)")
  Page<PriceEntity> findAll(Pageable pageable, PriceStatus filter, String currencyId);

  @PreAuthorize(
      "( T(org.apache.commons.collections4.CollectionUtils).isNotEmpty(#articleIds) && "
          + "#filter!=null && #filter.equals(T(shop.velox.price.api.dto.PriceStatus).ACTIVE) ) "
          + "|| @veloxAuthorizationEvaluator.hasGlobalOrCustomAdminAuthority(authentication, "
          + "T(shop.velox.price.service.PriceServiceConstants.Authorities).PRICE_ADMIN)")
  Page<PriceEntity> findAllByArticleIds(Pageable pageable, List<String> articleIds,
      PriceStatus filter, String currencyId);

  Optional<PriceEntity> findCheapestPriceForArticleAndQuantity(
      String articleId, BigDecimal quantity);

  Optional<PriceEntity> getAppliedPrice(String articleId, BigDecimal quantity);

  ArticleDto applyPrice(PriceEntity priceEntity, BigDecimal quantity);
}
