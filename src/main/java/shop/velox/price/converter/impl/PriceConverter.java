package shop.velox.price.converter.impl;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import shop.velox.commons.converter.Converter;
import shop.velox.price.api.dto.PriceDto;
import shop.velox.price.model.PriceEntity;

@Component
public class PriceConverter implements Converter<PriceEntity, PriceDto> {

  private static final Logger LOG = LoggerFactory.getLogger(PriceConverter.class);
  private final MapperFacade mapperFacade;

  public PriceConverter() {
    MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
    mapperFactory
        .classMap(PriceEntity.class, PriceDto.class)
        .byDefault()
        .register();

    mapperFacade = mapperFactory.getMapperFacade();
  }

  @Override
  public PriceDto convertEntityToDto(PriceEntity priceEntity) {
    PriceDto priceDto = getMapperFacade().map(priceEntity, PriceDto.class);
    LOG.debug("Converted {} to {}", priceEntity, priceDto);
    return priceDto;
  }

  @Override
  public PriceEntity convertDtoToEntity(PriceDto priceDto) {
    PriceEntity priceEntity = getMapperFacade().map(priceDto, PriceEntity.class);
    LOG.debug("Converted {} to {}", priceDto, priceEntity);
    return priceEntity;
  }

  public MapperFacade getMapperFacade() {
    return mapperFacade;
  }
}
