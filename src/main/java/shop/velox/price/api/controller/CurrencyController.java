package shop.velox.price.api.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import javax.validation.Valid;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.SortDefault;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import shop.velox.price.api.dto.CurrencyDto;

@Tag(name = "Currency", description = "the Currency API")
@RequestMapping("")
public interface CurrencyController {

  String CURRENCIES_PATH = "/currencies";

  @Operation(summary = "Get Currencies, sorted by isoCode ascending")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "successful operation", content = @Content(schema = @Schema(implementation = CurrencyDto.class))),
  })
  @GetMapping(value = CURRENCIES_PATH)
  ResponseEntity<Page<CurrencyDto>> getCurrencies(
      @SortDefault.SortDefaults({
          @SortDefault(sort = "isoCode", direction = Sort.Direction.ASC)}) Pageable pageable
  );

  @Operation(summary = "creates Currency")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "201", description = "returns the created currency", content = @Content(schema = @Schema(implementation = CurrencyDto.class))),
      @ApiResponse(responseCode = "422", description = "Missing or invalid data", content = @Content(schema = @Schema())),
  })
  @PostMapping(value = CURRENCIES_PATH, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
  ResponseEntity<CurrencyDto> createCurrency(
      @Parameter(hidden = true) @AuthenticationPrincipal(expression = "T(shop.velox.commons.security.utils.AuthUtils).principalToUserIdMapper.apply(#this)") String userId,
      @Parameter(description = "Currency to create. Cannot be empty.", required = true) @Valid @RequestBody CurrencyDto currency);

  @Operation(summary = "Get currency")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "successful operation", content = @Content(schema = @Schema(implementation = CurrencyDto.class))),
      @ApiResponse(responseCode = "404", description = "Currency for the given currency ID not found", content = @Content(schema = @Schema())),

  })
  @GetMapping(value = CURRENCIES_PATH + "/{id}")
  ResponseEntity<CurrencyDto> getCurrency(
      @Parameter(description = "ID of the currency. Cannot be empty.", required = true) @PathVariable("id") final String id
  );

  @Operation(summary = "Updates a currency.")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "successful operation", content = @Content(schema = @Schema(implementation = CurrencyDto.class))),
      @ApiResponse(responseCode = "404", description = "currency not found", content = @Content(schema = @Schema())),
      @ApiResponse(responseCode = "409", description = "id cannot be changed", content = @Content(schema = @Schema())),
      @ApiResponse(responseCode = "422", description = "Missing or invalid data", content = @Content(schema = @Schema()))
  })
  @PatchMapping(value = CURRENCIES_PATH + "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
  ResponseEntity<CurrencyDto> updateCurrency(
      @Parameter(description = "ID of the currency. Cannot be empty.", required = true) @PathVariable("id") String id,
      @Parameter(description = "Currency to update. Cannot be empty.", required = true) @Valid @RequestBody CurrencyDto currencyDto);

  @Operation(summary = "Delete currency by id")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "204", description = "successful operation"),
      @ApiResponse(responseCode = "404", description = "currency not found"),
      @ApiResponse(responseCode = "422", description = "currency can't be removed")
  })
  @DeleteMapping(value = CURRENCIES_PATH + "/{id}")
  ResponseEntity<Void> removeCurrency(
      @Parameter(description = "ID of the currency. Cannot be empty.", required = true) @PathVariable("id") final String id);

}
