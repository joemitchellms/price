package shop.velox.price.api.controller.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import shop.velox.price.api.controller.ArticlePriceController;
import shop.velox.price.api.dto.ArticleDto;
import shop.velox.price.api.dto.ArticleListDto;
import shop.velox.price.model.PriceEntity;
import shop.velox.price.service.PriceService;

@RestController
public class ArticlePriceControllerImpl implements ArticlePriceController {

  private static final Logger LOG = LoggerFactory.getLogger(ArticlePriceControllerImpl.class);

  private final PriceService priceService;

  public ArticlePriceControllerImpl(@Autowired PriceService priceService) {
    this.priceService = priceService;
  }

  @Override
  public ResponseEntity<ArticleListDto> getPrices(final ArticleListDto articleListDto) {
    List<ArticleDto> articlesWithPrices = articleListDto.getArticles().stream()
        .map(this::getPriceInternal)
        .filter(Optional::isPresent)
        .map(Optional::get)
        .collect(Collectors.toList());
    HttpStatus httpStatus =
        articleListDto.getArticles().size() == articlesWithPrices.size() ? HttpStatus.OK
            : HttpStatus.PARTIAL_CONTENT;
    return ResponseEntity.status(httpStatus).body(new ArticleListDto(articlesWithPrices));
  }

  @Override
  public ResponseEntity<ArticleDto> getPriceForQuantity(String id, BigDecimal quantity) {
    PriceEntity entityPage = priceService
        .findCheapestPriceForArticleAndQuantity(id, quantity)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

    ArticleDto articleDto = new ArticleDto(id, quantity,
        entityPage.getUnitPrice().multiply(quantity), entityPage.getUnitPrice());
    return ResponseEntity.status(HttpStatus.OK).body(articleDto);
  }

  private Optional<ArticleDto> getPriceInternal(ArticleDto articleDto) {

    String id = articleDto.getId();
    if (StringUtils.isEmpty(id)) {
      throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, "Article without id");
    }
    if (StringUtils.isEmpty(articleDto.getQuantity())) {
      throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY,
          "Article " + id + " without quantity");
    }

    Optional<ArticleDto> result = priceService
        .getAppliedPrice(articleDto.getId(), articleDto.getQuantity())
        .map(priceEntity -> priceService.applyPrice(priceEntity, articleDto.getQuantity()));

    if (LOG.isDebugEnabled()) {
      LOG.debug("getPriceInternal for id {} and quantity: {} returns {}",
          articleDto.getId(), articleDto.getQuantity(), result);
    }

    return result;
  }
}
