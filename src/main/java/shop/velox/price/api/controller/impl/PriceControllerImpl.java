package shop.velox.price.api.controller.impl;

import static org.apache.commons.collections4.CollectionUtils.isEmpty;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import shop.velox.commons.converter.Converter;
import shop.velox.price.api.controller.PriceController;
import shop.velox.price.api.dto.PriceDto;
import shop.velox.price.api.dto.PriceStatus;
import shop.velox.price.model.PriceEntity;
import shop.velox.price.service.PriceService;

@RestController
public class PriceControllerImpl implements PriceController {

  private static final Logger LOG = LoggerFactory.getLogger(PriceControllerImpl.class);

  private final PriceService priceService;

  private final Converter<PriceEntity, PriceDto> priceConverter;

  public PriceControllerImpl(@Autowired PriceService priceService,
      @Autowired Converter<PriceEntity, PriceDto> priceConverter) {
    this.priceService = priceService;
    this.priceConverter = priceConverter;
  }

  @Override
  public ResponseEntity<PriceDto> createPrice(final String userId, final PriceDto price) {
    LOG.info("{} is creating Price: {}", userId, price);
    PriceDto body = priceConverter
        .convertEntityToDto(priceService.createPrice(priceConverter.convertDtoToEntity(price)));
    LOG.info("{} has created Price: {}", userId, body);
    return ResponseEntity.status(HttpStatus.CREATED).body(body);
  }

  @Override
  public ResponseEntity<PriceDto> getPrice(final String id) {
    PriceEntity priceEntity = priceService.findById(id)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    PriceDto priceDto = priceConverter.convertEntityToDto(priceEntity);
    return ResponseEntity.status(HttpStatus.OK).body(priceDto);
  }

  @Override
  public ResponseEntity<Page<PriceDto>> getPrices(Pageable pageable, final List<String> articleIds,
      final PriceStatus filter, final String currencyId) {
    Page<PriceEntity> entitiesPage;
    Pageable responsePageable;
    if (isEmpty(articleIds)) {
      responsePageable = pageable;
      entitiesPage = priceService.findAll(responsePageable, filter, currencyId);
    } else {
      responsePageable = Pageable.unpaged();
      entitiesPage = priceService
          .findAllByArticleIds(responsePageable, articleIds, filter, currencyId);
    }

    Page<PriceDto> dtosPage = priceConverter.convert(responsePageable, entitiesPage);
    return ResponseEntity.ok(dtosPage);
  }

  @Override
  public ResponseEntity<PriceDto> updatePrice(final String id, final PriceDto priceDto) {
    LOG.info("updatePrice {}, {}", id, priceDto);
    PriceEntity priceEntity = priceService.update(id, priceConverter.convertDtoToEntity(priceDto));
    return ResponseEntity.ok(priceConverter.convertEntityToDto(priceEntity));
  }

  @Override
  public ResponseEntity<Void> removePrice(final String id) {
    LOG.info("removePrice {}", id);
    priceService.removePrice(id);
    return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
  }

  @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
  @ExceptionHandler(MethodArgumentNotValidException.class)
  public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
    return ex.getBindingResult()
        .getAllErrors()
        .stream()
        .collect(Collectors.toMap(
            error -> ((FieldError) error).getField(),
            ObjectError::getDefaultMessage));
  }
}
