package shop.velox.price.api.dto;

public enum PriceStatus {
  ACTIVE,
  UPCOMING,
  INACTIVE
}
