package shop.velox.price.dao;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import shop.velox.price.model.CurrencyEntity;
import shop.velox.price.model.PriceEntity;

public interface PriceRepository extends JpaRepository<PriceEntity, String>,
    JpaSpecificationExecutor<PriceEntity> {

  Optional<PriceEntity> findOneById(String id);

  Optional<PriceEntity> findOneByIdAndActiveIsTrue(String id);

  Optional<PriceEntity> findOneByArticleIdAndValidFromAndValidToAndMinQuantityAndUnitPriceAndCurrency(
      String articleId, LocalDateTime startDate, LocalDateTime endDate, BigDecimal minQuantity,
      BigDecimal unitPrice, CurrencyEntity currency);

  Optional<PriceEntity> findFirstByArticleIdAndActiveIsTrueAndMinQuantityLessThanEqualOrderByUnitPriceAsc(
      String articleId, BigDecimal quantity);

  Page<PriceEntity> findAllByArticleIdAndActiveIsTrueAndValidFromIsBeforeAndValidToIsAfterAndMinQuantityIsLessThanEqualOrderByUnitPriceAsc(
      Pageable pageable, String articleId, LocalDateTime startDate, LocalDateTime endDate,
      BigDecimal minQuantity);

  Page<PriceEntity> findAll(Pageable pageable);

  PriceEntity save(PriceEntity priceEntity);

}