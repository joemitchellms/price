package shop.velox.price.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import shop.velox.commons.model.AbstractEntity;

@Entity
@Table(uniqueConstraints = {
    @UniqueConstraint(name = PriceEntity.COMPOSITE_CONSTRAINT_NAME, columnNames = {"articleId",
        "validFrom", "validTo", "minQuantity", "unitPrice", "CURRENCY_PK"}),
})
public class PriceEntity extends AbstractEntity {

  public static final String COMPOSITE_CONSTRAINT_NAME = "Composite_Constraint";

  @PrePersist
  private void prePersistFunction() {
    if (StringUtils.isEmpty(id)) {
      id = UUID.randomUUID().toString();
    }
  }

  @Column(unique = true, nullable = false)
  private String id;

  @Column(nullable = false)
  private String articleId;

  @Column(nullable = false)
  private LocalDateTime validFrom;

  @Column(nullable = false)
  private LocalDateTime validTo;

  @Column(nullable = false)
  private BigDecimal minQuantity;

  @Column(nullable = false)
  private BigDecimal unitPrice;

  @Column(nullable = false)
  private boolean active;

  @ManyToOne(optional = false, fetch = FetchType.LAZY)
  @JoinColumn(name = "CURRENCY_PK", referencedColumnName = "PK")
  @NotNull
  private CurrencyEntity currency;

  public PriceEntity() {
    // For JPA
  }

  public PriceEntity(String articleId, LocalDateTime validFrom, LocalDateTime validTo,
      BigDecimal minQuantity, BigDecimal unitPrice, Boolean active, CurrencyEntity currency) {
    this.articleId = articleId;
    this.validFrom = ObjectUtils.defaultIfNull(validFrom, LocalDateTime.of(2000, 1, 1, 0, 0));
    this.validTo = ObjectUtils.defaultIfNull(validTo, LocalDateTime.of(2100, 1, 1, 0, 0));
    this.minQuantity = ObjectUtils.defaultIfNull(minQuantity, BigDecimal.ZERO);
    this.unitPrice = unitPrice;
    this.active = BooleanUtils.isNotFalse(active);
    this.currency = currency;
  }

  public PriceEntity(String articleId, LocalDateTime validFrom, LocalDateTime validTo,
      BigDecimal minQuantity, BigDecimal unitPrice, CurrencyEntity currency) {
    this(articleId, validFrom, validTo, minQuantity, unitPrice, null, currency);
  }

  public PriceEntity(String articleId, LocalDateTime validFrom, LocalDateTime validTo,
      BigDecimal unitPrice, CurrencyEntity currency) {
    this(articleId, validFrom, validTo, null, unitPrice, null, currency);
  }

  public PriceEntity(String articleId, BigDecimal unitPrice, CurrencyEntity currency) {
    this(articleId, null, null, null, unitPrice, null, currency);
  }

  public PriceEntity(String articleId, BigDecimal minQuantity, BigDecimal unitPrice,
      CurrencyEntity currency) {
    this(articleId, null, null, minQuantity, unitPrice, null, currency);
  }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
  }

  public String getId() {
    return id;
  }

  public String getArticleId() {
    return articleId;
  }

  public LocalDateTime getValidFrom() {
    return validFrom;
  }

  public LocalDateTime getValidTo() {
    return validTo;
  }

  public BigDecimal getMinQuantity() {
    return minQuantity;
  }

  public BigDecimal getUnitPrice() {
    return unitPrice;
  }

  public boolean isActive() {
    return active;
  }

  public void setActive(boolean active) {
    this.active = active;
  }

  public void setCurrency(CurrencyEntity currency) {
    this.currency = currency;
  }

  public CurrencyEntity getCurrency() {
    return currency;
  }

}
