package shop.velox.price.dao;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Optional;
import javax.annotation.Resource;
import javax.transaction.Transactional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import shop.velox.price.PriceApplicationTests;
import shop.velox.price.model.CurrencyEntity;

@ExtendWith(SpringExtension.class)
@Transactional
class CurrencyRepositoryIntegrationTest extends PriceApplicationTests {

  @Resource
  private CurrencyRepository currencyRepository;

  @Test
  public void findOneById() {
    // Given
    CurrencyEntity currency = currencyRepository
        .save(new CurrencyEntity("CHF", "Swiss franc", "CHF"));

    // When
    Optional<CurrencyEntity> result = currencyRepository.findOneById(currency.getId());

    // Then
    assertTrue(result.isPresent());
    assertThat(result.get(), hasProperty("isoCode", equalTo(currency.getIsoCode())));
  }

}
