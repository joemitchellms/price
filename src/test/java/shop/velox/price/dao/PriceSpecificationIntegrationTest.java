package shop.velox.price.dao;

import static java.time.LocalDateTime.now;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;

import java.math.BigDecimal;
import java.util.List;
import javax.annotation.Resource;
import javax.transaction.Transactional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import shop.velox.price.PriceApplicationTests;
import shop.velox.price.api.dto.PriceStatus;
import shop.velox.price.model.CurrencyEntity;
import shop.velox.price.model.PriceEntity;

@ExtendWith(SpringExtension.class)
@Transactional
class PriceSpecificationIntegrationTest extends PriceApplicationTests {

  @Resource
  private PriceRepository priceRepository;

  @Resource
  private CurrencyRepository currencyRepository;

  @Test
  void isArticleIdIn() {
    // Given
    CurrencyEntity currency = currencyRepository
        .save(new CurrencyEntity("CHF", "Swiss franc", "CHF"));

    PriceEntity price1 = priceRepository.save(new PriceEntity("001", new BigDecimal(10), currency));
    PriceEntity price2 = priceRepository.save(new PriceEntity("002", new BigDecimal(12), currency));

    assertThat(priceRepository.findAll(), contains(price1, price2));

    // When
    List<PriceEntity> result = priceRepository
        .findAll(PriceSpecification.isArticleIdIn(List.of(price1.getArticleId())));

    // Then
    assertThat(result, hasSize(1));
    assertThat(result, contains(price1));
  }

  @Test
  void isCurrencyEqual() {
    // Given
    CurrencyEntity currency1 = currencyRepository
        .save(new CurrencyEntity("CHF", "Swiss franc", "CHF"));
    CurrencyEntity currency2 = currencyRepository.save(new CurrencyEntity("EUR", "Euro", "€"));

    PriceEntity price1 = priceRepository
        .save(new PriceEntity("001", new BigDecimal(10), currency1));
    PriceEntity price2 = priceRepository
        .save(new PriceEntity("002", new BigDecimal(12), currency2));

    assertThat(priceRepository.findAll(), contains(price1, price2));

    // When
    List<PriceEntity> result = priceRepository
        .findAll(PriceSpecification.isCurrencyEqual(currency2.getId()));

    // Then
    assertThat(result, contains(price2));
  }

  @Test
  void hasStatusActive() {
    // Given
    CurrencyEntity currency1 = currencyRepository
        .save(new CurrencyEntity("CHF", "Swiss franc", "CHF"));

    PriceEntity price1 = priceRepository.save(
        new PriceEntity("001", now().minusDays(1), now().plusDays(1), new BigDecimal(0),
            new BigDecimal(10), true, currency1));
    PriceEntity price2 = priceRepository.save(
        new PriceEntity("002", now().minusDays(1), now().plusDays(1), new BigDecimal(0),
            new BigDecimal(10), false, currency1));
    PriceEntity price3 = priceRepository.save(
        new PriceEntity("003", now().minusDays(2), now().minusDays(1), new BigDecimal(0),
            new BigDecimal(10), true, currency1));
    PriceEntity price4 = priceRepository.save(
        new PriceEntity("004", now().plusDays(1), now().plusDays(2), new BigDecimal(0),
            new BigDecimal(10), true, currency1));

    assertThat(priceRepository.findAll(), contains(price1, price2, price3, price4));

    // When
    List<PriceEntity> result = priceRepository
        .findAll(PriceSpecification.hasStatus(PriceStatus.ACTIVE));

    // Then
    assertThat(result, contains(price1));
  }

  @Test
  void hasStatusInactive() {
    // Given
    CurrencyEntity currency1 = currencyRepository
        .save(new CurrencyEntity("CHF", "Swiss franc", "CHF"));

    PriceEntity price1 = priceRepository.save(
        new PriceEntity("001", now().minusDays(1), now().plusDays(1), new BigDecimal(0),
            new BigDecimal(10), true, currency1));
    PriceEntity price2 = priceRepository.save(
        new PriceEntity("002", now().minusDays(1), now().plusDays(1), new BigDecimal(0),
            new BigDecimal(10), false, currency1));
    PriceEntity price3 = priceRepository.save(
        new PriceEntity("003", now().minusDays(2), now().minusDays(1), new BigDecimal(0),
            new BigDecimal(10), true, currency1));
    PriceEntity price4 = priceRepository.save(
        new PriceEntity("004", now().plusDays(1), now().plusDays(2), new BigDecimal(0),
            new BigDecimal(10), true, currency1));

    assertThat(priceRepository.findAll(), contains(price1, price2, price3, price4));

    // When
    List<PriceEntity> result = priceRepository
        .findAll(PriceSpecification.hasStatus(PriceStatus.INACTIVE));

    // Then
    assertThat(result, contains(price2, price3));
  }

  @Test
  void hasStatusUpcoming() {
    // Given
    CurrencyEntity currency1 = currencyRepository
        .save(new CurrencyEntity("CHF", "Swiss franc", "CHF"));

    PriceEntity price1 = priceRepository.save(
        new PriceEntity("001", now().minusDays(1), now().plusDays(1), new BigDecimal(0),
            new BigDecimal(10), true, currency1));
    PriceEntity price2 = priceRepository.save(
        new PriceEntity("002", now().minusDays(1), now().plusDays(1), new BigDecimal(0),
            new BigDecimal(10), false, currency1));
    PriceEntity price3 = priceRepository.save(
        new PriceEntity("003", now().minusDays(2), now().minusDays(1), new BigDecimal(0),
            new BigDecimal(10), true, currency1));
    PriceEntity price4 = priceRepository.save(
        new PriceEntity("004", now().plusDays(1), now().plusDays(2), new BigDecimal(0),
            new BigDecimal(10), true, currency1));

    assertThat(priceRepository.findAll(), contains(price1, price2, price3, price4));

    // When
    List<PriceEntity> result = priceRepository
        .findAll(PriceSpecification.hasStatus(PriceStatus.UPCOMING));

    // Then
    assertThat(result, contains(price4));
  }

  @Test
  void testEmptySpecification() {
    // Given
    CurrencyEntity currency1 = currencyRepository
        .save(new CurrencyEntity("CHF", "Swiss franc", "CHF"));

    PriceEntity price1 = priceRepository.save(
        new PriceEntity("001", new BigDecimal(10), currency1));

    // When
    List<PriceEntity> result = priceRepository.findAll(
        PriceSpecification.isArticleIdIn(null)
            .and(PriceSpecification.hasStatus(null)
                .and(PriceSpecification.isCurrencyEqual(null))));

    // Then
    assertThat(result, contains(price1));
  }

}