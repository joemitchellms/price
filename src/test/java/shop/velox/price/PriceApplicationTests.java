package shop.velox.price;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

@SpringBootTest
@ActiveProfiles({"embeddeddb", "localauth"})
@TestMethodOrder(OrderAnnotation.class)
public class PriceApplicationTests {

  @Test
  void contextLoads() {
  }

}
